#include <unistd.h>
#include <stdlib.h>
void* mymalloc(size_t size)
{
	void *ret;
	size_t realsize;
	realsize=size+sizeof(p_block);
	ret=sbrk(realsize);
	if(ret==-1)
	{
		perror("there was an error");
		return (NULL);
	}
	else return ret;
} 