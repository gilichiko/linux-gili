#include <unistd.h>
#include <stdlib.h>
struct metadata_block
{
	size_t size;
	struct metadata_block* next;
	int free;	
};

typedef struct metadata_block* p_block;

static struct metadata_block start.size=0;
static struct metadata_block strat.next=NULL;
static struct metadata_block strat.free=0;
