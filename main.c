#include "ceaser.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
	if (argc < 4)
	{
		printf("usage: %s <mode> <message> <key>\n",argv[0]);
		exit(1);
	}
	int mode;
	if(argv[1][0] == 'd') 	mode=1;
	else mode = -1;
	int key = atoi(argv[3]);
	printf("the input is %s\n",argv[2]);
	char * input = shift_string(argv[2],mode * key);
	printf("%s\n",input);

	return 0;
}
