#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
int getnrow(FILE * file)
{
	int i=0;
	while(fgetc(file)!=EOF)
	{
		if(fgetc(file)=='\n') i++;
	}
	fseek(file,0,SEEK_SET);
	return i;
}

int main(int argc, char ** argv)
{
	FILE * file=fopen(argv[1],"rw");
	int count=0,row,nrow=0,tabs=0,bits=0;
	if(file)
	{
		nrow=getnrow(file);
		srand(time(NULL));
		row=(rand()%nrow)+1;
		printf("this is row:  %d \n",row);
		while(count<row)
		{
			if(fgetc(file)=='\n')
			{
				 count++;
			}
			bits++;
		}
		fseek(file,tabs+bits,SEEK_SET);
		while(count==row&&fgetc(file)!=EOF)
		{
			fseek(file,tabs+bits,SEEK_SET);
			printf("%c",fgetc(file));
			tabs++;	
			if(fgetc(file)=='\n') count++;
		}
		printf("\n");	
		fclose(file);
	}
	else
	{
		printf("opening of the file failed\n");
		return(0);
	}

	return 0;
}
