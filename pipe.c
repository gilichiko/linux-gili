#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <linux/limits.h>
#include "LineParser.h"
void main()
{
	int fd[2];
	char *str;
	pipe(fd);
	int child_pid=fork();
	if(child_pid==0)
	{
		str="magshimim";
		printf("im the child %s -str \n",str);
		
		write(fd[1],str,sizeof("magshimim"));
	}	
	else
	{
		wait(child_pid);
		printf("im the father \n");
		read(fd[0],str,sizeof("magshimim"));
		printf("father str %s \n",str);
	}
}
